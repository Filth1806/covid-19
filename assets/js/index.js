$(document).ready(function() {
    $('.sidebar').load('views/base/sidebar.html');
    $('#main-panel').load('views/statistics.html');
    //$('.navbar').load('views/base/navbar.html');  
    $.getScript('./assets/demo/demo.js', function() {
        //alert('hecho');
        demo.initDashboardPageCharts();
    });

    $(document).on('click', 'a', (function(){
        var $temp = $.trim($(this).text());
        //alert($.trim($temp));
        if($temp == "Estadísticas")
        {
            $('#main-panel').load('views/statistics.html');        
            $.getScript('./assets/demo/demo.js', function() {
                //alert('hecho');
                demo.initDashboardPageCharts();
            });
            $("li").removeClass("active");
            $("#statistics").addClass("active");
        }
        else  if($temp == "¿Qué es?")
        {
            $('#main-panel').load('views/whatis.html');        
            $("li").removeClass("active");
            $("#whatis").addClass("active");
        }
        else  if($temp == "Origen")
        {
            $('#main-panel').load('views/origin.html');        
            $("li").removeClass("active");
            $("#origin").addClass("active");
        }else  if($temp == "Tipos de brotes")
        {
            $('#main-panel').load('views/types.html');        
            $("li").removeClass("active");
            $("#types").addClass("active");
        }else  if($temp == "¿A quiénes afecta?")
        {
            $('#main-panel').load('views/who.html');        
            $("li").removeClass("active");
            $("#who").addClass("active");
        }
        else  if($temp == "Síntomas")
        {
            $('#main-panel').load('views/symptoms.html');        
            $("li").removeClass("active");
            $("#symptoms").addClass("active");
        }
        else  if($temp == "Diagnóstico")
        {
            $('#main-panel').load('views/diagnostic.html');        
            $("li").removeClass("active");
            $("#diagnostic").addClass("active");
        }
        else  if($temp == "¿Cómo se propaga el virus?")
        {
            $('#main-panel').load('views/how1.html');        
            $("li").removeClass("active");
            $("#how1").addClass("active");
        }
        else  if($temp == "¿Cómo reducir el riesgo?")
        {
            $('#main-panel').load('views/how2.html');        
            $("li").removeClass("active");
            $("#how2").addClass("active");
        }
        else  if($temp == "Prevención")
        {
            $('#main-panel').load('views/prevention.html');        
            $("li").removeClass("active");
            $("#prevention").addClass("active");
        }
        else  if($temp == "Tratamientos")
        {
            $('#main-panel').load('views/treatments.html');        
            $("li").removeClass("active");
            $("#treatments").addClass("active");
        }
        else  if($temp == "Propagación global")
        {
            $('#main-panel').load('views/propagation.html');        
            $("li").removeClass("active");
            $("#propagation").addClass("active");
        }
        else  if($temp == "Referencias bibliográficas")
        {
            $('#main-panel').load('views/references.html');        
            $("li").removeClass("active");
            $("#references").addClass("active");
        }
    }));
    
    $(document).on('click', '#statistics', 
        function() {
            // Javascript method's body can be found in assets/js/demos.js
            //alert("hello"); 
            
        }
    );
});